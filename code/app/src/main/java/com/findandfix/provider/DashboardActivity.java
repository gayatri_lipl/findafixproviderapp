package com.findandfix.provider;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.findandfix.provider.fragment.HomeFragment;
import com.findandfix.provider.fragment.LocationFragment;
import com.findandfix.provider.fragment.MyFragment;
import com.findandfix.provider.fragment.OfferFragment;
import com.findandfix.provider.fragment.OrderFragment;
import com.findandfix.provider.fragment.ServiceFragment;
import com.findandfix.provider.locationfragment.AddLocationFragment;
import com.findandfix.provider.locationfragment.AddLocationTwoFragment;
import com.findandfix.provider.providerapp.R;
import com.findandfix.provider.servicefragment.AddServiceFragment;
import com.findandfix.provider.servicefragment.AssignRateFragment;
import com.findandfix.provider.servicefragment.AssignServiceFragment;
import com.findandfix.provider.servicefragment.PackageFragment;
import com.findandfix.provider.slidemenu.MenuSlider;
import com.findandfix.provider.slidemenu.SlidingMenu;

public class DashboardActivity extends AppCompatActivity {
    private TextView tv_header,tv_loginheader;
    private Activity activity;
    private View headerlayout;
    private LinearLayout ll_login;
    private ImageView im_menu,im_header;
    public MenuSlider menu1;
    private SlidingMenu menu;
    private MyFragment currentFragment;
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private boolean doubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        activity = DashboardActivity.this;
        initComponent();
        initListner();
    }

    private void initComponent() {
        menu1 = new MenuSlider(this, this, menu);
        menu = menu1.slidemenu(this);

        headerlayout = findViewById(R.id.headerlayout);
        tv_header = headerlayout.findViewById(R.id.tv_header);
        tv_loginheader = headerlayout.findViewById(R.id.tv_loginheader);
        ll_login = headerlayout.findViewById(R.id.ll_login);
        im_menu = headerlayout.findViewById(R.id.im_menu);
        im_header = headerlayout.findViewById(R.id.im_header);

        im_menu.setVisibility(View.VISIBLE);
        tv_loginheader.setText(getResources().getString(R.string.auto_up));
        im_header.setImageDrawable(getResources().getDrawable(R.mipmap.auto_gp));


        this.fragmentManager = getSupportFragmentManager();
        changeFragment(MyFragment.HOMEFRAGMENT, "");
    }

    private void initListner() {
        im_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menu.toggle();
            }
        });
    }

    public  void changeFragment(MyFragment myFragment, Object object) {

        switch (myFragment) {
            case HOMEFRAGMENT:
                this.currentFragment = MyFragment.HOMEFRAGMENT;
                tv_header.setText(getResources().getString(R.string.welcome)+" "+"JOHN AUTO");
                fragment = new HomeFragment();
                break;
            case LOCATIONFRAGMENT:
                this.currentFragment = MyFragment.LOCATIONFRAGMENT;
                tv_header.setText(getResources().getString(R.string.manage_location));
                fragment = new LocationFragment();
                break;
            case ADDLOCATIONFRAGMENT:
                this.currentFragment = MyFragment.ADDLOCATIONFRAGMENT;
                tv_header.setText(getResources().getString(R.string.manage_location));
                fragment = new AddLocationFragment();
                break;
            case ADDLOCATIONTWOFRAGMENT:
                this.currentFragment = MyFragment.ADDLOCATIONTWOFRAGMENT;
                tv_header.setText(getResources().getString(R.string.manage_location));
                fragment = new AddLocationTwoFragment();
                break;
            case SERVICEFRAGMENT:
                this.currentFragment = MyFragment.SERVICEFRAGMENT;
                tv_header.setText(getResources().getString(R.string.manage_service));
                fragment = new ServiceFragment();
                break;
            case OFFERFRAGMENT:
                this.currentFragment = MyFragment.OFFERFRAGMENT;
                tv_header.setText(getResources().getString(R.string.manage_offer));
                fragment = new OfferFragment();
                break;
            case ADDSERVICEFRAGMENT:
                this.currentFragment = MyFragment.ADDSERVICEFRAGMENT;
                tv_header.setText(getResources().getString(R.string.manage_service_add));
                fragment = new AddServiceFragment();
                break;
            case ASSIGNSERVICEFRAGMENT:
                this.currentFragment = MyFragment.ASSIGNSERVICEFRAGMENT;
                tv_header.setText(getResources().getString(R.string.assign_service));
                fragment = new AssignServiceFragment();
                break;
            case ASSIGNRATEFRAGMENT:
                this.currentFragment = MyFragment.ASSIGNRATEFRAGMENT;
                tv_header.setText(getResources().getString(R.string.assign_rate_service));
                fragment = new AssignRateFragment();
                break;
            case PACKAGEFRAGMENT:
                this.currentFragment = MyFragment.PACKAGEFRAGMENT;
                tv_header.setText(getResources().getString(R.string.define_package));
                fragment = new PackageFragment();
                break;
            case ORDERFRAGMENT:
                this.currentFragment = MyFragment.ORDERFRAGMENT;
                tv_header.setText(getResources().getString(R.string.manage_order));
                fragment = new OrderFragment();
                break;

            default:
                this.currentFragment = MyFragment.HOMEFRAGMENT;
                fragment = new HomeFragment();
                break;
        }
        Bundle bundle = new Bundle();
        bundle.putString("screen_name", "SCREEN_" + currentFragment);
        this.transaction = this.fragmentManager.beginTransaction();
        this.transaction.replace(R.id.frame_container, fragment);
        this.transaction.commit();

    }

    @Override
    public void onBackPressed() {
        switch (currentFragment) {
            case HOMEFRAGMENT:
                if (!this.doubleBackToExitPressedOnce) {
                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_LONG).show();
                    new Handler().postDelayed(new BackPraced(), 2000);
                    break;
                } else {
                    moveTaskToBack(true);
                    //super.onBackPressed();
                }
                return;
            case LOCATIONFRAGMENT:
                changeFragment(MyFragment.HOMEFRAGMENT, "");
                break;
            case ADDLOCATIONFRAGMENT:
                changeFragment(MyFragment.LOCATIONFRAGMENT, "");
                break;
            case ADDLOCATIONTWOFRAGMENT:
                changeFragment(MyFragment.ADDLOCATIONFRAGMENT, "");
                break;
            case SERVICEFRAGMENT:
                changeFragment(MyFragment.HOMEFRAGMENT, "");
                break;
            case OFFERFRAGMENT:
                changeFragment(MyFragment.HOMEFRAGMENT, "");
                break;
            case ADDSERVICEFRAGMENT:
                changeFragment(MyFragment.SERVICEFRAGMENT, "");
                break;
            case ASSIGNSERVICEFRAGMENT:
                changeFragment(MyFragment.SERVICEFRAGMENT, "");
                break;
            case ASSIGNRATEFRAGMENT:
                changeFragment(MyFragment.SERVICEFRAGMENT, "");
                break;
            case PACKAGEFRAGMENT:
                changeFragment(MyFragment.SERVICEFRAGMENT, "");
                break;
            case ORDERFRAGMENT:
                changeFragment(MyFragment.HOMEFRAGMENT, "");
                break;
        }
    }

    class BackPraced implements Runnable {
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
    }
}
