package com.findandfix.provider.servicefragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.findandfix.provider.providerapp.R;
import com.findandfix.provider.utility.ImageCompressionAsyncTask;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

/**
 * Created by lipl-01 on 4/6/2018.
 */

public class PackageFragment extends Fragment {

    private Activity activity;
    private TextView tv_serviceselect,tv_durationservice,tv_addpackagefeature,tv_submit;
    private EditText et_packagename,et_addescription;
    private ImageView im_uploadimage;
    public final int SELECT_FROM_GALLERY = 1;
    public final int CAPTURE_FROM_CAMERA = 2;
    private String IMAGE_DIRECTORY_NAME = "Provider_Image";
    private String imagePath = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_package, container, false);
        activity = getActivity();
        initComponent(view);
        initListner();
        return view;
    }

    private void initComponent(View view) {
        tv_serviceselect = view.findViewById(R.id.tv_serviceselect);
        tv_durationservice = view.findViewById(R.id.tv_durationservice);
        tv_addpackagefeature = view.findViewById(R.id.tv_addpackagefeature);
        tv_submit = view.findViewById(R.id.tv_submit);
        et_packagename = view.findViewById(R.id.et_packagename);
        et_addescription = view.findViewById(R.id.et_addescription);
        im_uploadimage = view.findViewById(R.id.im_uploadimage);
    }

    private void initListner() {
        tv_serviceselect.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });

        et_packagename.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        tv_durationservice.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });

        et_addescription.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        im_uploadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageOptions(SELECT_FROM_GALLERY, CAPTURE_FROM_CAMERA);
            }
        });
        tv_addpackagefeature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void showImageOptions(final int GALLARY, final int CAMERA) {
        CharSequence colors[] = new CharSequence[]{"From Gallery", "Take A Photo"};
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Image");
        builder.setItems(colors,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        switch (which) {
                            case 0:
                                selectImage(GALLARY);
                                break;

                            case 1:
                                captureImage(CAMERA);
                                break;

                            default:
                                break;
                        }
                    }
                });
        builder.show();

    }
    public void selectImage(final int code) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            // Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(activity,android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,android.Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(activity,new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},code);
                } else {
                    ActivityCompat.requestPermissions(activity,new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},code);
                }
            }else{
                processSelectionOfImage(code);
            }
        } else {
            processSelectionOfImage(code);
        }

    }

    public void captureImage(int code) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, code);
                } else {
                    ActivityCompat.requestPermissions(activity,new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE,android.Manifest.permission.READ_EXTERNAL_STORAGE}, code);
                }
            }else{
                processCaptureOfImage(code);
            }
        } else {
            processCaptureOfImage(code);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == SELECT_FROM_GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                processSelectionOfImage(requestCode);
            } else {
                Toast.makeText(activity, "Please grant permission to continue...", Toast.LENGTH_SHORT).show();
            }
        }
        if (requestCode == CAPTURE_FROM_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                processCaptureOfImage(requestCode);
            } else {
                Toast.makeText(activity, "Please grant permission to continue...", Toast.LENGTH_SHORT).show();

            }
        }
    }

    public void processSelectionOfImage(int code) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        Uri fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        try {
            intent.putExtra("return-data", true);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), code);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*
 * Capturing Camera Image will lauch camera app requrest image capture
 */
    public void processCaptureOfImage(int code) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                Uri fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent, code);
            }

            else {
                File file = new File(getOutputMediaFileUri(MEDIA_TYPE_IMAGE).getPath());
                Uri photoUri = FileProvider.getUriForFile(activity, activity.getPackageName() + ".provider", file);
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                if (intent.resolveActivity(activity.getPackageManager()) != null) {
                    startActivityForResult(intent, code);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));

    }

    private File getOutputMediaFile(int type) {
        Log.e("before directory", "yes");
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), IMAGE_DIRECTORY_NAME);
        Log.e("before directory", mediaStorageDir.getPath() + "");
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            Log.e("storage directory", "yes");
            if (!mediaStorageDir.mkdirs()) {
                Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        imagePath = mediaFile.getAbsolutePath();
        return mediaFile;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_FROM_CAMERA) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String selectedImagePath = imagePath;
                    imagePath = null;
                    long fileSizeInBytes = new File(selectedImagePath).length();
                    // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    if (fileSizeInKB>500) {
                        new ImageCompressionAsyncTask(activity, selectedImagePath,new ImageCompressionAsyncTask.CompressImage() {
                            @Override
                            public void onCompressionFinished(String path) {
                                Glide.with(activity).load(path).into(im_uploadimage);
                            }
                        }).execute();
                    }else{
                        Glide.with(activity).load(imagePath).into(im_uploadimage);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(activity,
                        "Sorry! You cancel", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(activity,
                        "Sorry! Failed to capture image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        else if (requestCode == SELECT_FROM_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                try {

                    Uri selectedImageUri = data.getData();
                    String selectedImagePath = getPath(selectedImageUri);

                    long fileSizeInBytes = new File(selectedImagePath).length();
                    // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
                    long fileSizeInKB = fileSizeInBytes / 1024;
                    if (fileSizeInKB>500) {
                        new ImageCompressionAsyncTask(activity, selectedImagePath,new ImageCompressionAsyncTask.CompressImage() {
                            @Override
                            public void onCompressionFinished(String path) {
                                Glide.with(activity).load(path).into(im_uploadimage);
                            }
                        }).execute();
                    }else{
                        Glide.with(activity).load(selectedImagePath).into(im_uploadimage);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(activity,
                        "Sorry! You cancel", Toast.LENGTH_SHORT)
                        .show();
            } else {
                // failed to capture image
                Toast.makeText(activity,
                        "Sorry! Failed to select image", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }

    public String  getPath(Uri uri) {
        // just some safety built in
        if (uri == null) {
            // TODO perform some logging or showMessage user feedback
            return null;
        }
        // try to retrieve the image from the media store first
        // this will only work for images selected from gallery
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        // this is our fallback here
        return uri.getPath();
    }
}
