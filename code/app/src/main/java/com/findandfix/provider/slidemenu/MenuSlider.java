package com.findandfix.provider.slidemenu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.findandfix.provider.providerapp.R;


public class MenuSlider {
    public Context mContext;
    private SlidingMenu menu;
     Activity activity;
    private LinearLayout ll_home , ll_about_us ,ll_membership,ll_review,ll_contact_us , ll_dashboard , ll_search,ll_blog,layout_drawer;
    private TextView tv_login , headertext;
    private  ImageView profileImage;
    private ProgressDialog dialog;


    public MenuSlider(Activity mactivity, Context _context, SlidingMenu _menu) {
        mContext = _context;
        menu = _menu;
        activity = mactivity;
        slidemenu(activity);

    }


    public SlidingMenu slidemenu(final Activity activity) {
        menu = new SlidingMenu(mContext);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setBehindOffset(50);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(activity, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.lay_drawer_menu);

        initComponent();
        initListner();
        return menu;
    }

    private void initComponent()
    {
        layout_drawer = (LinearLayout)menu.findViewById(R.id.layout_drawer);
    }

    private void initListner() {
    }

}
