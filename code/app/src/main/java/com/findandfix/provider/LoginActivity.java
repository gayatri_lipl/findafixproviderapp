package com.findandfix.provider;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.findandfix.provider.providerapp.R;

public class LoginActivity extends AppCompatActivity {
    private EditText et_email,et_password;
    private TextView tv_login,tv_signup,tv_header;
    private Activity activity;
    private View headerlayout;
    private LinearLayout ll_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        activity = LoginActivity.this;
        initComponent();
        initListner();
    }

    private void initComponent() {
        headerlayout = findViewById(R.id.headerlayout);
        tv_header = headerlayout.findViewById(R.id.tv_header);
        ll_login = headerlayout.findViewById(R.id.ll_login);
        ll_login.setVisibility(View.INVISIBLE);
        tv_header.setText(getResources().getString(R.string.login));

        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);

        tv_login = findViewById(R.id.tv_login);
        tv_signup = findViewById(R.id.tv_signup);

    }

    private void initListner() {
        et_email.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });

        et_password.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity,DashboardActivity.class));
            }
        });
        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity,RegistrationActivity.class));
            }
        });
    }
}
