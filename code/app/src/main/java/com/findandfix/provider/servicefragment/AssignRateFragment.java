package com.findandfix.provider.servicefragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.findandfix.provider.providerapp.R;

/**
 * Created by lipl-01 on 4/6/2018.
 */

public class AssignRateFragment extends Fragment {

    private Activity activity;
    private TextView tv_selectservice,tv_selectpackage,tv_carmaker,tv_carmodel,tv_cartype,tv_submit;
    private EditText et_assignrate;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_assignrate, container, false);
        activity = getActivity();
        initComponent(view);
        initListner();
        return view;
    }

    private void initComponent(View view) {
        tv_selectservice = view.findViewById(R.id.tv_selectservice);
        tv_selectpackage = view.findViewById(R.id.tv_selectpackage);
        tv_carmaker = view.findViewById(R.id.tv_carmaker);
        tv_carmodel = view.findViewById(R.id.tv_carmodel);
        tv_cartype = view.findViewById(R.id.tv_cartype);
        tv_submit = view.findViewById(R.id.tv_submit);
        et_assignrate = view.findViewById(R.id.et_assignrate);
    }

    private void initListner() {
        tv_selectservice.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        tv_selectpackage.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        tv_carmaker.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        tv_carmodel.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        tv_cartype.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        et_assignrate.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

}
