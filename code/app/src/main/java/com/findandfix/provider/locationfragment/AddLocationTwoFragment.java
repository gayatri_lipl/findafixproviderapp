package com.findandfix.provider.locationfragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.findandfix.provider.providerapp.R;

/**
 * Created by lipl-01 on 4/6/2018.
 */

public class AddLocationTwoFragment extends Fragment {

    private Activity activity;
    private TextView tv_submit;
    private EditText et_locationmanagername,et_emailid,et_conatctnumber,et_alternatenumber,et_comment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_addlocationtwo, container, false);
        activity = getActivity();
        initComponent(view);
        initListner();
        return view;
    }

    private void initComponent(View view) {
        et_locationmanagername = view.findViewById(R.id.et_locationmanagername);
        et_emailid = view.findViewById(R.id.et_emailid);
        et_conatctnumber = view.findViewById(R.id.et_conatctnumber);
        et_alternatenumber = view.findViewById(R.id.et_alternatenumber);
        et_comment = view.findViewById(R.id.et_comment);
        tv_submit = view.findViewById(R.id.tv_submit);
    }

    private void initListner() {
        et_locationmanagername.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        et_emailid.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        et_conatctnumber.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        et_alternatenumber.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });

        et_comment.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

}
