package com.findandfix.provider.locationfragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.findandfix.provider.DashboardActivity;
import com.findandfix.provider.fragment.MyFragment;
import com.findandfix.provider.providerapp.R;

/**
 * Created by lipl-01 on 4/6/2018.
 */

public class AddLocationFragment extends Fragment {

    private Activity activity;
    private TextView tv_addmore,tv_next;
    private EditText et_locationname,et_locationid,et_city,et_country,et_address1,et_address2,et_starttime,et_endtime;
    private CheckBox chk_all;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_addlocation, container, false);
        activity = getActivity();
        initComponent(view);
        initListner();
        return view;
    }

    private void initComponent(View view) {
        tv_addmore = view.findViewById(R.id.tv_addmore);
        tv_next = view.findViewById(R.id.tv_next);
        et_locationname = view.findViewById(R.id.et_locationname);
        et_locationid = view.findViewById(R.id.et_locationid);
        et_city = view.findViewById(R.id.et_city);
        et_country = view.findViewById(R.id.et_country);
        et_address1 = view.findViewById(R.id.et_address1);
        et_address2 = view.findViewById(R.id.et_address2);
        et_starttime = view.findViewById(R.id.et_starttime);
        et_endtime = view.findViewById(R.id.et_endtime);
        chk_all = view.findViewById(R.id.chk_all);
    }

    private void initListner() {
        et_locationname.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        et_locationid.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        et_city.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        et_country.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });

        et_address1.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        et_address2.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        et_starttime.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        et_endtime.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) activity).changeFragment(MyFragment.ADDLOCATIONTWOFRAGMENT, "");
            }
        });
    }

}
