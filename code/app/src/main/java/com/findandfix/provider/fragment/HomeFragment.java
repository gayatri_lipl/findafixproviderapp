package com.findandfix.provider.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.findandfix.provider.DashboardActivity;
import com.findandfix.provider.providerapp.R;

/**
 * Created by lipl-01 on 4/6/2018.
 */

public class HomeFragment extends Fragment {
    private Activity activity;
    private LinearLayout ll_location,ll_service,ll_offer,ll_profile,ll_report,ll_order;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        activity = getActivity();
        initComponent(view);
        initListner();
        return view;
    }

    private void initComponent(View view) {
        ll_location = view.findViewById(R.id.ll_location);
        ll_service = view.findViewById(R.id.ll_service);
        ll_offer = view.findViewById(R.id.ll_offer);
        ll_profile = view.findViewById(R.id.ll_profile);
        ll_report = view.findViewById(R.id.ll_report);
        ll_order = view.findViewById(R.id.ll_order);
    }

    private void initListner() {
        ll_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) activity).changeFragment(MyFragment.LOCATIONFRAGMENT, "");
            }
        });
        ll_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) activity).changeFragment(MyFragment.SERVICEFRAGMENT, "");
            }
        });
        ll_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) activity).changeFragment(MyFragment.OFFERFRAGMENT, "");
            }
        });
        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        ll_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        ll_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) activity).changeFragment(MyFragment.ORDERFRAGMENT, "");
            }
        });

    }

}
