package com.findandfix.provider;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.findandfix.provider.providerapp.R;

public class RegistrationStepTwo extends AppCompatActivity {
    private EditText et_verification_code,et_password,et_reenter_password;
    private TextView tv_submit,tv_sucess_msg,tv_receiv_confirm,tv_header;
    private Activity activity;
    private View headerlayout;
    private LinearLayout ll_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_step2);
        activity = RegistrationStepTwo.this;
        initComponent();
        initListner();
    }

    private void initComponent() {
        headerlayout = findViewById(R.id.headerlayout);
        tv_header = headerlayout.findViewById(R.id.tv_header);
        ll_login = headerlayout.findViewById(R.id.ll_login);

        et_verification_code = findViewById(R.id.et_verification_code);
        et_password = findViewById(R.id.et_password);
        et_reenter_password = findViewById(R.id.et_reenter_password);

        tv_submit = findViewById(R.id.tv_submit);
        tv_sucess_msg = findViewById(R.id.tv_sucess_msg);
        tv_receiv_confirm = findViewById(R.id.tv_receiv_confirm);
    }

    private void initListner() {
        ll_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity,LoginActivity.class));
            }
        });
        et_verification_code.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });

        et_password.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });

        et_reenter_password.setOnFocusChangeListener( new View.OnFocusChangeListener(){
            public void onFocusChange( View view, boolean hasfocus){
                if(hasfocus){
                    view.setBackgroundResource(R.drawable.editwithborder);
                }
                else{
                    view.setBackgroundColor(getResources().getColor(R.color.whitecolor));
                }
            }
        });
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(activity,RegistrationStepTwo.class));
            }
        });
    }

}
