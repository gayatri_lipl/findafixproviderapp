package com.findandfix.provider.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.findandfix.provider.DashboardActivity;
import com.findandfix.provider.providerapp.R;

/**
 * Created by lipl-01 on 4/6/2018.
 */

public class ServiceFragment extends Fragment {
    private Activity activity;
    private LinearLayout ll_addservice,ll_assignservice,ll_assignrate,ll_package;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        activity = getActivity();
        initComponent(view);
        initListner();
        return view;
    }

    private void initComponent(View view) {
        ll_addservice = view.findViewById(R.id.ll_addservice);
        ll_assignservice = view.findViewById(R.id.ll_assignservice);
        ll_assignrate = view.findViewById(R.id.ll_assignrate);
        ll_package = view.findViewById(R.id.ll_package);
    }

    private void initListner() {
        ll_addservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) activity).changeFragment(MyFragment.ADDSERVICEFRAGMENT, "");
            }
        });
        ll_assignservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) activity).changeFragment(MyFragment.ASSIGNSERVICEFRAGMENT, "");
            }
        });
        ll_assignrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) activity).changeFragment(MyFragment.ASSIGNRATEFRAGMENT, "");
            }
        });
        ll_package.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) activity).changeFragment(MyFragment.PACKAGEFRAGMENT, "");
            }
        });
    }

}
