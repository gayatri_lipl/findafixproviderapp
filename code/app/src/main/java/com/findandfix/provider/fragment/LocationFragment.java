package com.findandfix.provider.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.findandfix.provider.DashboardActivity;
import com.findandfix.provider.providerapp.R;

/**
 * Created by lipl-01 on 4/6/2018.
 */

public class LocationFragment extends Fragment {

    private Activity activity;
    private TextView tv_addlocation;
    private ListView lv_location;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_location, container, false);
        activity = getActivity();
        initComponent(view);
        initListner();
        return view;
    }

    private void initComponent(View view) {
        tv_addlocation = view.findViewById(R.id.tv_addlocation);
        lv_location = view.findViewById(R.id.lv_location);
    }

    private void initListner() {
        tv_addlocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DashboardActivity) activity).changeFragment(MyFragment.ADDLOCATIONFRAGMENT, "");
            }
        });
    }

}
